
$(function(){
        var stickyHeaderTop = $('#stickytypeheader').offset().top;
 
        $(window).scroll(function(){
                if( $(window).scrollTop() > stickyHeaderTop ) {
                        $('#stickytypeheader').css({position: 'fixed', top: '0px', backgroundColor: 'white', boxShadow: '1px 1px 2px rgba(0, 0, 0, 0.1)', zIndex: '130'});
                        $('#hamburger-container').css({position: 'fixed', top: '-38px'});
                        $('#book-now-box').css({position: 'fixed', top: '-40px'});
                        $('#main-logo-box').css({position: 'fixed', top: '0px', width: '10%', marginLeft: '45%', marginTop: '-10px'});
                        $('#sticky').css('display', 'block');
                } else {
                        $('#stickytypeheader').css({position: 'static', top: '0px', 
                            backgroundColor: 'rgba(255, 255, 255, 0)',  boxShadow: '1px 1px 2px rgba(0, 0, 0, 0)', zIndex: '150'});
                        $('#hamburger-container').css({top: '0px'});
                        $('#book-now-box').css({top: '0px'});
                        $('#main-logo-box').css({position: 'fixed', top: '20px', width: '30%', marginLeft: '35%', marginTop: '0px'});
                        $('#sticky').css('display', 'none');
                }
        });
  });

$( function() {
  $( "#datepicker" ).datepicker();
} );

$(document).ready(function(){

    $('.main-page-book-now-box').click(function() {
        document.getElementsByClassName("book-now-area")[0].style.right = 0;
        document.getElementsByClassName("book-now-overflow")[0].style.display = 'block';
    })

    $('.book-now-overflow').click(function(){
        document.getElementsByClassName("book-now-area")[0].style.right = '-30%';
        document.getElementsByClassName("book-now-overflow")[0].style.display = 'none';
    })
})
