
/* changing landing and events backgrounds */
/* changing landing and events backgrounds */

$(document).ready(function() {

	var landingbg = document.getElementsByClassName('main-page-first-container')[0];
	var eventsbg = document.getElementsByClassName('events-by-type')[0];

	$('#hoverlanding1').mouseover(function() {
		landingbg.style.backgroundImage = "url('images/landing_hotel3.jpg')";
	})

	$('#hoverlanding2').mouseover(function() {
		landingbg.style.backgroundImage = "url('images/landing_beach.jpg')";
	})

	$('#hoverlanding3').mouseover(function() {
		landingbg.style.backgroundImage = "url('images/landing_rooms2.jpg')";
	})

	$('#hoverlanding4').mouseover(function() {
		landingbg.style.backgroundImage = "url('images/landing_bar2.jpg')";
	})

	$('#hoverlanding5').mouseover(function() {
		landingbg.style.backgroundImage = "url('images/landing_spa2.jpg')";
	})

	$('#event1').mouseover(function() {
		eventsbg.style.backgroundImage = "url('images/events_background_bbq.jpg')";
	})

	$('#event2').mouseover(function() {
		eventsbg.style.backgroundImage = "url('images/events_background_salsa.jpg')";
	})

	$('#event3').mouseover(function() {
		eventsbg.style.backgroundImage = "url('images/events_background_yoga.jpg')";
	})

	$('#event4').mouseover(function() {
		eventsbg.style.backgroundImage = "url('images/events_background_football.jpg')";
	})


/* transparent container equaled to footer */
/* transparent container equaled to footer */

	var transparentbox = document.getElementsByClassName('transparent-container')[0];
	var footer = document.getElementsByClassName('main-page-fourth-container')[0];
	var transparentboxheight = $(transparentbox).css("height");
	var footerheight = $(footer).css("height");

	if (transparentboxheight > footerheight) {
		$(transparentbox).height(footerheight);
	}
	else {
		$(transparentbox).height(footerheight);
	}



/* show hidden menu function */
/* show hidden menu function */

	var hiddenmenu = document.getElementsByClassName('hidden-menu')[0];
	var menubutton = document.getElementsByClassName('main-page-hamburger-container')[0];
	var booknowbox = document.getElementsByClassName('main-page-book-now-box')[0];
	var menubar = document.getElementsByClassName('fa-bars')[0];
	var menuclose = document.getElementsByClassName('fa-times')[0];
	var menuspan1 = document.getElementsByClassName('menuspan')[0];
	var menuspan2 = document.getElementsByClassName('menuspan')[1];

	$(menubutton).click(function(){

		var li = $(this);
		var activecls = 'on';
		var isactive = li.is('.'+activecls);

		$('.'+activecls).removeClass(activecls);
		$(hiddenmenu).css('margin-top', '-100vh');
		$(menubar).show();
		$(menuspan1).show();
		$(menuspan2).hide();
		$(menuclose).hide();

		if(isactive) {
			
		}
		else {
			$(this).addClass('on');
			$(hiddenmenu).css('margin-top', '0vh');
			$(menuclose).show();
			$(menuspan2).show()
			$(menubar).hide();
			$(menuspan1).hide();

		}

	})

	/* show mobile hidden menu */

	$('.headermobile').click(function(){
		$(hiddenmenu).css('margin-left', '0%');

		var li = $(this);
		var activecls = 'on';
		var isactive = li.is('.'+activecls);

		$('.'+activecls).removeClass(activecls);
		$(hiddenmenu).css('margin-left', '-100%');

		if(isactive) {
			
		}
		else {
			$(this).addClass('on');
			$(hiddenmenu).css('margin-left', '0%');
		}
	})

	/* show desktop book now area */

	$('.main-page-book-now-box, .book-now-box').click(function() {
        document.getElementsByClassName("book-now-area")[0].style.right = 0;
        document.getElementsByClassName("book-now-overflow")[0].style.display = 'block';
    })

    $('.book-now-overflow').click(function(){
        document.getElementsByClassName("book-now-area")[0].style.right = '-30%';
        document.getElementsByClassName("book-now-overflow")[0].style.display = 'none';
    })



	/* show mobile book now area */

	var booknowmobile = document.getElementsByClassName('book-now-area')[0];

	$('.booknowmobile').click(function() {
		$(booknowmobile).css('margin-top', '220px');

		var li = $(this);
		var activecls = 'on';
		var isactive = li.is('.'+activecls);

		$('.'+activecls).removeClass(activecls);
		$(booknowmobile).css('margin-top', '-100vh');

		if(isactive) {
			
		}
		else {
			$(this).addClass('on');
			$(booknowmobile).css('margin-top', '120px');
		}
	})

})