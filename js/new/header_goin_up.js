
$(function(){
        var stickyHeaderTop = $('#header').offset().top;
 
        $(window).scroll(function(){
                if( $(window).scrollTop() > stickyHeaderTop ) {
                        $('#header').css({position: 'fixed', top: '0px', backgroundColor: 'white', boxShadow: '1px 1px 2px rgba(0, 0, 0, 0.1)', zIndex: '130'});
                } else {
                        $('#header').css({position: 'static', top: '0px', 
                            backgroundColor: 'rgba(255, 255, 255, 0)',  boxShadow: '1px 1px 2px rgba(0, 0, 0, 0)', zIndex: '150'});
                }
        });
  });
