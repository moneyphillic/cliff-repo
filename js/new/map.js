function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
          scrollwheel: false,
          center: {lat: 42.805, lng: 27.885},
          zoom: 10,
          styles: [
            {
              featureType: 'all',
              stylers: [
              { saturation: -80 }
              ]
            },
            {
              featureType: 'road.arterial',
              elementType: 'geometry',
              stylers: [
                { hue: '#00ffee' },
                { saturation: 50 }
              ]
            },{
              featureType: 'poi.business',
              elementType: 'labels',
              stylers: [
                { visibility: 'off' }
              ]
            }
          ]
        });
        var myLatLng = {lat: 42.8057533, lng: 27.8868333};

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });

      }