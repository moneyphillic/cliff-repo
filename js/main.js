/* DATE PICKER JS */

$( function() {
	$( "#datepicker" ).datepicker();
});

$(document).ready(function() {


	/* MAIN PAGE CHANGING BACKGROUNDS */

	var landingbg = document.getElementsByClassName('main-page-first-container')[0];
	var eventsbg = document.getElementsByClassName('events-by-type')[0];

	$('#hoverlanding1').mouseover(function() {
		landingbg.style.backgroundImage = "url('images/landing_hotel3.jpg')";
	})

	$('#hoverlanding2').mouseover(function() {
		landingbg.style.backgroundImage = "url('images/landing_beach.jpg')";
	})

	$('#hoverlanding3').mouseover(function() {
		landingbg.style.backgroundImage = "url('images/landing_rooms2.jpg')";
	})

	$('#hoverlanding4').mouseover(function() {
		landingbg.style.backgroundImage = "url('images/landing_bar2.jpg')";
	})

	$('#hoverlanding5').mouseover(function() {
		landingbg.style.backgroundImage = "url('images/landing_spa2.jpg')";
	})

	$('#event1').mouseover(function() {
		eventsbg.style.backgroundImage = "url('images/events_background_bbq.jpg')";
	})

	$('#event2').mouseover(function() {
		eventsbg.style.backgroundImage = "url('images/events_background_salsa.jpg')";
	})

	$('#event3').mouseover(function() {
		eventsbg.style.backgroundImage = "url('images/events_background_yoga.jpg')";
	})

	$('#event4').mouseover(function() {
		eventsbg.style.backgroundImage = "url('images/events_background_football.jpg')";
	})


/* transparent container equaled to footer */
/* transparent container equaled to footer */

	var transparentbox = document.getElementsByClassName('transparent-container')[0];
	var footer = document.getElementsByClassName('main-page-fourth-container')[0];
	var transparentboxheight = $(transparentbox).css("height");
	var footerheight = $(footer).css("height");

	if (transparentboxheight > footerheight) {
		$(transparentbox).height(footerheight);
	}
	else {
		$(transparentbox).height(footerheight);
	}

	/* sticky header function */

    $(function(){
        var stickyHeaderTop = $('#stickytypeheader').offset().top;
 
        $(window).scroll(function(){

            if( $(window).scrollTop() > stickyHeaderTop ) {
                    $('#stickytypeheader').css({position: 'fixed', top: '0px', backgroundColor: 'white', boxShadow: '1px 1px 2px rgba(0, 0, 0, 0.1)', 
                    	zIndex: '130'});
                    $('#hamburger-container').css({position: 'fixed', top: '10px'});
                    $('#book-now-box').css({position: 'fixed', top: '10px'});
                    $('#main-logo-box').css({position: 'fixed', top: '0px', width: '10%', left: '45%', marginTop: '-10px'});
                    $('#sticky').css('display', 'block');
            } else {
                    $('#stickytypeheader').css({position: 'static', top: '0px', 
                        backgroundColor: 'rgba(255, 255, 255, 0)',  boxShadow: '1px 1px 2px rgba(0, 0, 0, 0)', zIndex: '150'});
                    $('#hamburger-container').css({top: '8%'});
                    $('#book-now-box').css({top: '8%'});
                    $('#main-logo-box').css({position: 'fixed', top: '20px', width: '30%', left: '35%', marginTop: '0px'});
                    $('#sticky').css('display', 'none');
            }
        });

        /* datepicker */

		$( function() {
		  $( "#datepicker" ).datepicker();
		});

  });

/* faq dynamic JS */

$(document).ready(function() {

    $('.each-faq').click(function() {

        let el = $(this);
        let id = el.data('id');
        let activeCls = 'active-faq';
        let isActive = el.is('.'+activeCls);

        $('.'+activeCls).removeClass(activeCls);
        $('.faq-hidden-text').hide();
        console.log(isActive);

        if (isActive) {

        }

        else {

            $('#hiddentext'+id).show();
            el.addClass(activeCls);
        }
    })
});

	/* show hidden menu function */
	/* show hidden menu function */

	var hiddenmenu = document.getElementsByClassName('hidden-menu')[0];
	var menubutton = document.getElementsByClassName('hamburger-himself')[0];
	var booknowbox = document.getElementsByClassName('book-now-box')[0];
	var menubar = document.getElementsByClassName('fa-bars')[0];
	var menuclose = document.getElementsByClassName('fa-times')[0];

	$(menubutton).click(function(){

		var li = $(this);
		var activecls = 'on';
		var isactive = li.is('.'+activecls);

		$('.'+activecls).removeClass(activecls);
		$(hiddenmenu).css('margin-top', '-100vh');
		$(menubar).show();
		$(menuclose).hide();

		if(isactive) {
			
		}
		else {
			$(this).addClass('on');
			$(hiddenmenu).css('margin-top', '0vh');
			$(menuclose).show();
			$(menubar).hide();

		}
	})

	/* show mobile hidden menu */

	$('.headermobile').click(function(){
		$(hiddenmenu).css('margin-left', '0%');

		var li = $(this);
		var activecls = 'on';
		var isactive = li.is('.'+activecls);

		$('.'+activecls).removeClass(activecls);
		$(hiddenmenu).css('margin-left', '-100%');

		if(isactive) {
			
		}
		else {
			$(this).addClass('on');
			$(hiddenmenu).css('margin-left', '0%');
		}
	})

	/* show desktop book now area */

	$('.book-now-box').click(function() {
        document.getElementsByClassName("book-now-area")[0].style.right = 0;
        document.getElementsByClassName("book-now-overflow")[0].style.display = 'block';
    })

    $('.book-now-overflow').click(function(){
        document.getElementsByClassName("book-now-area")[0].style.right = '-30%';
        document.getElementsByClassName("book-now-overflow")[0].style.display = 'none';
    })

    /* show mobile book now area */

	var booknowmobile = document.getElementsByClassName('book-now-area')[0];

	$('.booknowmobile').click(function() {
		$(booknowmobile).css('margin-top', '220px');

		var li = $(this);
		var activecls = 'on';
		var isactive = li.is('.'+activecls);

		$('.'+activecls).removeClass(activecls);
		$(booknowmobile).css('margin-top', '-100vh');

		if(isactive) {
			
		}
		else {
			$(this).addClass('on');
			$(booknowmobile).css('margin-top', '120px');
		}
	})

})

/* SWIPER SLIDER JS */

var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 30,
        loop: true
    });


/* STYLED MAP JS */

function initMap() {

    var map = new google.maps.Map(document.getElementById('map'), {
      scrollwheel: false,
      center: {lat: 42.805, lng: 27.885},
      zoom: 10,
      styles: [
        {
          featureType: 'all',
          stylers: [
          { saturation: -80 }
          ]
        },
        {
          featureType: 'road.arterial',
          elementType: 'geometry',
          stylers: [
            { hue: '#00ffee' },
            { saturation: 50 }
          ]
        },{
          featureType: 'poi.business',
          elementType: 'labels',
          stylers: [
            { visibility: 'off' }
          ]
        }
      ]
    });
    var myLatLng = {lat: 42.8057533, lng: 27.8868333};

    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: 'Hello World!'
    });

}
